

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Launcher {
	public Launcher() {
		
		//if 32 or 64bit
		//System.setProperty("org.lwjgl.util.Debug", "true"); //???
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		final JFrame frame = new JFrame();
		frame.setTitle("Fibber 28");
		frame.setSize(250, 120); //310 200
		frame.setResizable(true);
		frame.setLocationRelativeTo(null);
		//frame.setLayout(new GridBagLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final JPanel panel = new JPanel();
		frame.add(panel);
		
		final Checkbox full = new Checkbox("Fullscreen", null, Settings.DEBUG ? false : true); 
		
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		final JTextField width = new JTextField();
		width.setText(""+(Settings.DEBUG ? 1280 : gd.getDisplayMode().getWidth()));
		
		final JTextField height = new JTextField();
		height.setText(""+(Settings.DEBUG ? 720 : gd.getDisplayMode().getHeight()));
		
		JButton launch = new JButton("Start");
		launch.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					Integer.parseInt(width.getText());
				}catch(Exception e){
					width.setBackground(Color.RED);
					return;
				}
				try{
					Integer.parseInt(height.getText());
				}catch(Exception e){
					height.setBackground(Color.RED);
					return;
				}
				frame.dispose();
				
				Settings s = new Settings();
				s.fullscreen = full.getState();
				s.resolution = new Dimension(Integer.parseInt(width.getText()), Integer.parseInt(height.getText()));
				/*s.full = full.getState();
				s.aa = aa.getSelectedValue();
				s.width = Integer.parseInt(width.getText());
				s.height = Integer.parseInt(height.getText());
				s.volume = volume.getValue() / 10.0;
				s.radioVolume = radioVolume.getValue() / 100.0;
				s.name = name.getText().replace("o", "o").replace("a", "a").replace("a","a");*/
				new Raytracer(s).run();
			}
		});
		
		
		panel.add(full);
		panel.add(new JLabel("Resolution:"));
		panel.add(width);
		panel.add(new JLabel("x"));
		panel.add(height);
		//panel.add(new JLabel("Music Volume:"));
		//panel.add(musicVolume);
		panel.add(launch);
		
		
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		new Launcher();
	}
}
