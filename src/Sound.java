

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class Sound {

	public static final SoundFile MUSIC = new SoundFile("music.wav", 1.0);

	
	private SoundFile currentMusic;
	private double soundVolume;
	private double musicVolume;
	private Clip musicClip;

	public Sound() { // 0 - 1
		double volume = 1;
		this.soundVolume = volume;//soundVolume;
		this.musicVolume = volume; //musicVolume;
		setMusic(MUSIC);
	}
	
	public Clip play(SoundFile sound) {
		return play(sound, soundVolume);
	}
	

	
	public Clip play(SoundFile sound, double volume) {
		try {
			Clip clip = (Clip) AudioSystem.getLine(sound.info);
			clip.open(sound.af, sound.audio, 0, sound.size);
			FloatControl volumeControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
			volumeControl.setValue((float) (20 * Math.log10(volume * sound.volume * soundVolume)));
			clip.start();
			return clip;
		} catch (Exception e) {
			//JOptionPane.showMessageDialog(null, "play sound " + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	public void setMusic(SoundFile music) {
		
		if (currentMusic == music) {
			return;
		}
		if (musicClip != null) {
			musicClip.stop();
		}

		currentMusic = music;
		try {

			musicClip = (Clip) AudioSystem.getLine(music.info);
			musicClip.open(music.af, music.audio, 0, music.size);
			musicClip.loop(Clip.LOOP_CONTINUOUSLY);
			FloatControl volume = (FloatControl) musicClip.getControl(FloatControl.Type.MASTER_GAIN);
			volume.setValue((float) (20 * Math.log10(musicVolume * music.volume)));
			musicClip.start();
			
			/*new Thread() {
				@Override
				public void run() {
					while(true){
						Vector3f disv = new Vector3f(game.getPlayerPos());
						disv.sub(game.getNextNodePos());
						float dis = disv.length();
						float g = 1 - dis / 100;
						if (g > 1) {
							g = 1;
						} else if (g < 0){
							g = 0;
						}
						try{
							volume.setValue((float) (20 * Math.log10(musicVolume * music.volume * g)));
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
			}.start();*/
		} catch (Exception e) {
			//JOptionPane.showMessageDialog(null, "set music " + e.getMessage());
			e.printStackTrace();
		}
		
	}

	public void stopMusic() {
		currentMusic = null;
		musicClip.stop();
	}

}